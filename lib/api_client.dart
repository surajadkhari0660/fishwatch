import 'dart:convert';

import 'package:http/http.dart';

class ApiClient {
  static const String endpoint = 'https://www.fishwatch.gov/api/';

  getData({String species = ''}) async {
    final Response response = await get(Uri.parse(endpoint + species));
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception(response.reasonPhrase);
    }
  }
}
