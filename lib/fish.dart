import 'package:fishwatch/logic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class FishHome extends ConsumerWidget {
  const FishHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
    final _data = ref.watch(fishDataProvider);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Fish'),
      ),
      body: _data.when(
        data: (_data) {
          return ListView(
            children: _data.map((e) => Text(e.management)).toList(),
          );
        },
        error: (err, s) => Center(
          child: Text(err.toString()),
        ),
        loading: () => const CircularProgressIndicator(),
      ),
    );
  }
}
