import 'package:fishwatch/logic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class FishStateUI extends StatelessWidget {
  const FishStateUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Fish State'),
      ),
      body: ListView(
        children: const [
          ConsumerFish(),
        ],
      ),
    );
  }
}

class ConsumerFish extends ConsumerWidget {
  const ConsumerFish({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
    ref.listen(fishLogicProvider, ((prev, next) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: ElevatedButton(
            onPressed: () {},
            child: Text(prev.toString()),
          ),
        ),
      );
    }));
    FishState _state = ref.watch(fishLogicProvider);
    return ListView(
      shrinkWrap: true,
      children: [
        if (_state is FishInitial)
          ElevatedButton(
            onPressed: () {
              ref.read(fishLogicProvider.notifier).getData();
            },
            child: const Text('Load'),
          ),
        if (_state is FishLoading)
          const Center(
            child: CircularProgressIndicator(),
          ),
        if (_state is FishError)
          Center(
            child: Text(_state.error),
          ),
        if (_state is FishLoaded)
          ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: [
              ..._state.fishes.map(
                (e) => Text(e.management),
              )
            ],
          ),
      ],
    );
  }
}
